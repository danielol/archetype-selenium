package pages;

import org.openqa.selenium.WebDriver;

public class LoginPage extends AbstractPage {

	private static final String ENTRAR = "entrar";
	private static final String SENHA = "senha";
	private static final String USUARIO = "usuario";

	public LoginPage(WebDriver driver) {
		super(driver);
	}

	public LoginPage visita(String url) {
		driver.get(url);
		return this;
	}

	public HomePage autenticar(String usuario, String senha) {
		preencherCampoPorName(USUARIO, usuario);
		preencherCampoPorName(SENHA, senha);
		findElementById(ENTRAR).click();
		return new HomePage(driver);
	}

}
