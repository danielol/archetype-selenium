package pages;

import org.openqa.selenium.WebDriver;

import utils.Utils;

public class FaixaDeNumeracaoPage extends AbstractPage {

	private static final String ID_LABEL_CONTRATO = "#j_idt104";
	private static final String IDCONTRATO = "contratoCadastro";
	private static final String ID_BTN_SALVAR = "btnSalvar";
	private static final String CLASS_MSG_ERRO = "ui-messages-error-summary";

	protected FaixaDeNumeracaoPage(WebDriver driver) {
		super(driver);
	}

	public String retornandoMsgErroValidacao() {

		salvandoCadastroVazio(ID_BTN_SALVAR);
		Utils.isVisibleByClassName(CLASS_MSG_ERRO, driver, 10);
		String mensagemErro = findElementByClassName(CLASS_MSG_ERRO).getText();
		return mensagemErro;
	}

	public String pegaLabel() {
		return findElementById(ID_LABEL_CONTRATO).getText();
	}

}
