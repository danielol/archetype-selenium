package pages;

import org.openqa.selenium.WebDriver;

import utils.PropertyUtil;

public class HomePage extends AbstractPage {

	private static final String TITULO_PRINCIPAL = "tituloPrincipal";
	private static final String MENU_CADASTROS_ID_COMBO_BTN = "menuCadastro_button";
	private static final String MENU_CADASTROS_XPATH_FORMATO_EMBALAGEM = "/html/body/div[5]/ul/li[4]/a";
	private static final String MENU_CADASTROS_XPATH_PRODUTOS = "//*[@id=\"menuCadastro_menu\"]/ul/li[3]/a/span";
	private static final String MENU_CADASTROS_XPATH_FAIXA_DE_NUMERACAO = "//*[@id=\"menuCadastro_menu\"]/ul/li[5]/a";

	protected HomePage(WebDriver driver) {
		super(driver);
	}

	public boolean isValida() {
		return temTituloBemVindo();
	}

	private boolean temTituloBemVindo() {
		return findElementByClassName(TITULO_PRINCIPAL).getText().contains(PropertyUtil.getMsg("label.inicio"));
	}

	public FormatoEmbalagemPage navegarParaFormatoEmbalagemPage() {
		findElementById(MENU_CADASTROS_ID_COMBO_BTN).click();
		findElementByXPath(MENU_CADASTROS_XPATH_FORMATO_EMBALAGEM).click();
		return new FormatoEmbalagemPage(driver);
	}

	public ProdutoPage selecionaMenuConsultaProduto() {
		findElementById(MENU_CADASTROS_ID_COMBO_BTN).click();
		findElementByXPath(MENU_CADASTROS_XPATH_PRODUTOS).click();
		return new ProdutoPage(driver);

	}

	public FaixaDeNumeracaoPage selecionaMenuFaixaNumeracao() {
		findElementById(MENU_CADASTROS_ID_COMBO_BTN).click();
		findElementByXPath(MENU_CADASTROS_XPATH_FAIXA_DE_NUMERACAO).click();
		return new FaixaDeNumeracaoPage(driver);
	}

}
