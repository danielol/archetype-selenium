package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public abstract class AbstractPage {

	protected WebDriver driver;
	
	protected AbstractPage(WebDriver driver) {
		this.driver = driver;
	}

	protected WebElement findElementById(String id) {
		return driver.findElement(By.id(id));
	}

	protected WebElement findElementByName(String name) {
		return driver.findElement(By.name(name));
	}

	protected WebElement findElementByClassName(String className) {
		return driver.findElement(By.className(className));
	}

	protected WebElement findElementByCssSeletor(String cssSeletor) {
		return driver.findElement(By.cssSelector(cssSeletor));
	}

	protected WebElement findElementByXPath(String xpath) {
		return driver.findElement(By.xpath(xpath));
	}

	protected WebElement findElementByTagName(String tagName) {
		return driver.findElement(By.tagName(tagName));
	}

	protected WebElement findElementByLinkText(String linkText) {
		return driver.findElement(By.linkText(linkText));
	}

	protected WebElement findElementByPartialLinkText(String partialLinkText) {
		return driver.findElement(By.partialLinkText(partialLinkText));
	}

	protected void preencherCampoPorId(String id, String value) {
		findElementById(id).click();
		findElementById(id).clear();
		findElementById(id).sendKeys(value);
	}

	protected void preencherCampoPorName(String name, String value) {
		findElementByName(name).click();
		findElementByName(name).clear();
		findElementByName(name).sendKeys(value);
	}
	
	protected void salvandoCadastroVazio(String name) {
		findElementByName(name).click();
	}

	protected void novoSalvaPorId(String idNovo, String idSalva) {
		findElementById(idNovo).click();
		findElementById(idSalva).click();
	}
}
