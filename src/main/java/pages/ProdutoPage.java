package pages;

import org.openqa.selenium.WebDriver;

import utils.Utils;

public class ProdutoPage extends AbstractPage {

	private static final String ID_SIGLAPRODUTO = "siglaproduto";
	private static final String ID_CODIGOPRODUTO = "codigoproduto";
	private static final String ID_BTN_NOVO = "j_idt50";
	private static final String ID_BTN_PESQUISAR = "btnPesquisar";
	private static final String TESTE2 = "teste";
	private static final String ID_SIGLA = "sigla";
	private static final String ID_NOME = "nome";
	private static final String ID_CODIGO_PRODUTO = "codigoProduto";
	private static final String ID_BTN_SALVAR = "j_idt84";
	private static final String MENSAGEM_CAMPO_OBRIGATORIO = "//*[@id=\"msgs\"]/div/ul/li/span";
	private static final String MENSAGEM_CODIGO_PRODUTO_EXISTENTE = "//*[@id=\"msgs\"]/div/ul/li/span";

	protected ProdutoPage(WebDriver driver) {
		super(driver);
	}

	public void preencheCamposPesquisa() {
		preencherCampoPorId(ID_CODIGO_PRODUTO, TESTE2);
		preencherCampoPorId(ID_NOME, TESTE2);
		preencherCampoPorId(ID_SIGLA, TESTE2);
		findElementById(ID_BTN_PESQUISAR).click();
	}

	public void cadastraProdutos() {
		findElementById(ID_BTN_NOVO).click();
		preencherCampoPorId(ID_CODIGOPRODUTO, "764540");
		preencherCampoPorId(ID_SIGLAPRODUTO, "TES");
		findElementById(ID_BTN_SALVAR).click();
	}

	public void validandoCamposObrigatoriosDoCadastro() {
		novoSalvaPorId(ID_BTN_NOVO, ID_BTN_SALVAR);
	}

	public String pegandoMensagemValidacao() {
		Utils.isVisibleByXpath(MENSAGEM_CAMPO_OBRIGATORIO, driver, 5);
		return findElementByXPath(MENSAGEM_CAMPO_OBRIGATORIO).getText();
	}

	public String pegandoMensagemValidacaoCodigoProdutoExistente() {
		findElementById(ID_BTN_NOVO).click();
		preencherCampoPorId(ID_CODIGOPRODUTO, "764540");
		findElementById(ID_BTN_SALVAR).click();
		Utils.isVisibleByXpath(MENSAGEM_CODIGO_PRODUTO_EXISTENTE, driver, 5);
		return findElementById(MENSAGEM_CODIGO_PRODUTO_EXISTENTE).getText();
	}
}
