package pages;

import org.openqa.selenium.WebDriver;

import utils.PropertyUtil;

public class FormatoEmbalagemPage extends AbstractPage {

	/* T�tulo */
	private static final String TITULO_XPATH_LABEL_PAGINA = "/html/body/div[1]/div[3]/form/h4/span";

	/* Consulta */
	private static final String FILTRO_XPATH_LABEL_TIPO_EMBALAGEM = "//*[@id=\"j_idt40\"]";
	private static final String FILTRO_XPATH_LABEL_STATUS = "//*[@id=\"j_idt44\"]";
	private static final String FILTRO_XPATH_LABEL_PRODUTO = "//*[@id=\"j_idt48\"]";
	private static final String TABELA_XPATH_LABEL_TIPO_EMBALAGEM = "/html/body/div[1]/div[3]/form/div[4]/div/div/div[1]/table/thead/tr/th[1]/span";
	private static final String TABELA_XPATH_LABEL_ALTURA = "/html/body/div[1]/div[3]/form/div[4]/div/div/div[1]/table/thead/tr/th[2]/span";
	private static final String TABELA_XPATH_LABEL_LARGURA = "/html/body/div[1]/div[3]/form/div[4]/div/div/div[1]/table/thead/tr/th[3]/span";
	private static final String TABELA_XPATH_LABEL_COMPRIMENTO = "/html/body/div[1]/div[3]/form/div[4]/div/div/div[1]/table/thead/tr/th[4]/span";
	private static final String TABELA_XPATH_LABEL_PESO = "/html/body/div[1]/div[3]/form/div[4]/div/div/div[1]/table/thead/tr/th[5]/span";
	private static final String TABELA_XPATH_LABEL_PRODUTO = "/html/body/div[1]/div[3]/form/div[4]/div/div/div[1]/table/thead/tr/th[6]/span";
	private static final String TABELA_XPATH_LABEL_STATUS = "/html/body/div[1]/div[3]/form/div[4]/div/div/div[1]/table/thead/tr/th[7]/span";
	private static final String TABELA_XPATH_LABEL_ACOES = "/html/body/div[1]/div[3]/form/div[4]/div/div/div[1]/table/thead/tr/th[8]/span";
	private static final String FILTRO_ID_COMBO_TIPO_FORMATO_EMBALAGEM = "tipoFormatoEmbalagem";
	private static final String FILTRO_ID_COMBO_ATIVO = "ativo";
	private static final String FILTRO_CID_OMBO_PRODUTO = "nomeProduto";

	/* Cadastro/Editar */
	private static final String CADASTRO_XPATH_LABEL_BREADCUMB = "/html/body/div[1]/div[3]/div/ul/li/a";
	private static final String CADASTRO_XPATH_LABEL_MENSAGEM_OBRIGATORIO = "/html/body/div[1]/div[3]/div/div/div/div/span";
	private static final String CADASTRO_XPATH_LABEL_INCLUIR_EMBALAGEM = "/html/body/div[1]/div[3]/div/div/div/form/div[1]/div[1]/span";
	private static final String CADASTRO_XPATH_LABEL_TIPO_EMBALAGEM = "";
	private static final String CADASTRO_XPATH_LABEL_ALTURA = "";
	private static final String CADASTRO_XPATH_LABEL_LARGURA = "";
	private static final String CADASTRO_XPATH_LABEL_DESCRICAO = "";
	private static final String CADASTRO_XPATH_LABEL_COMPRIMENTO = "";
	private static final String CADASTRO_XPATH_LABEL_DIMENSAO = "/html/body/div[1]/div[3]/div/div/div/form/div[1]/div[2]/table/tbody/tr[3]/td[2]/span";
	private static final String CADASTRO_ID_CHECKBOX_DIMENSAO = "dimensaoVariavelCad";
	private static final String CADASTRO_LXPATH_LABEL_DESCRICAO = "//*[@id=\"j_idt102\"]";
	private static final String CADASTRO_ID_TEXT_AREA_DESCRICAO = "obs";
	private static final String CADASTRO_XPATH_LABEL_SELECIONAR_PRODUTOS = "/html/body/div[1]/div[3]/div/div/div/form/div[2]/div[1]/span";
	private static final String CADASTRO_XPATH_LABEL_CODIGO_PRODUTO = "//*[@id=\"j_idt106\"]";
	private static final String CADASTRO_XPATH_TABELA_LABEL_CODIGO_PRODUTO = "/html/body/div[1]/div[3]/div/div/div/form/div[2]/div[2]/div/div/div/div[1]/table/thead/tr/th[1]/span";
	private static final String CADASTRO_XPATH_TABELA_LABEL_PRODUTO = "/html/body/div[1]/div[3]/div/div/div/form/div[2]/div[2]/div/div/div/div[1]/table/thead/tr/th[2]/span";
	private static final String CADASTRO_XPATH_TABELA_LABEL_PESO = "/html/body/div[1]/div[3]/div/div/div/form/div[2]/div[2]/div/div/div/div[1]/table/thead/tr/th[3]/span";
	private static final String CADASTRO_XPATH_TABELA_LABEL_REMOVER = "/html/body/div[1]/div[3]/div/div/div/form/div[2]/div[2]/div/div/div/div[1]/table/thead/tr/th[4]/span";
	private static final String CADASTRO_ID_BTN_INCLUIR = "btnIncluirProduto";
	private static final String CADASTRO_ID_BTN_SALVAR = "btnSalvar";
	private static final String CADASTRO_ID_BTN_CANCELAR = "j_idt118";
	/**/

	public FormatoEmbalagemPage(WebDriver driver) {
		super(driver);
	}

	public boolean recuperarLabelTitulo() {
		return findElementByXPath(TITULO_XPATH_LABEL_PAGINA).getText()
				.contains(PropertyUtil.getMsg("label.menu.manter.formatoEmbalagem"));
	}

	public String recuperarLabelFiltroTipoEmbalagem() {
		return findElementByXPath(FILTRO_XPATH_LABEL_TIPO_EMBALAGEM).getText();
	}

}
