package utils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.io.Files;

public abstract class Utils {
	private static WebDriverWait wait;

	public static void gerarDriver(WebDriver driver, Integer tempo) {
		wait = new WebDriverWait(driver, tempo);
	}

	public static void isVisibleById(String locator, WebDriver driver, Integer tempo) {
		gerarDriver(driver, tempo);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(locator)));
	}

	public static void isVisibleByClassName(String locator, WebDriver driver, Integer tempo) {
		gerarDriver(driver, tempo);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(locator)));
	}

	public static void isVisibleByXpath(String locator, WebDriver driver, Integer tempo) {
		gerarDriver(driver, tempo);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
	}

	public static void isClickableById(String locator, WebDriver driver, Integer tempo) {
		gerarDriver(driver, tempo);
		wait.until(ExpectedConditions.elementToBeClickable(By.id(locator)));
	}

	public static void isClickableByClassName(String locator, WebDriver driver, Integer tempo) {
		gerarDriver(driver, tempo);
		wait.until(ExpectedConditions.elementToBeClickable(By.className(locator)));
	}

	public static void isClickableByXpath(String locator, WebDriver driver, Integer tempo) {
		gerarDriver(driver, tempo);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
	}

	public static void isLocatedById(String locator, WebDriver driver, Integer tempo) {
		gerarDriver(driver, tempo);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(locator)));
	}

	public static void isLocatedByClassName(String locator, WebDriver driver, Integer tempo) {
		gerarDriver(driver, tempo);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.className(locator)));
	}

	public static void isLocatedByXpath(String locator, WebDriver driver, Integer tempo) {
		gerarDriver(driver, tempo);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(locator)));
	}

	public static void takeScreenshot(String fileName, WebDriver driver) {

		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		criarDiretorio();
		try {
			Files.copy(scrFile,
					new File("C:\\SeleniumScreenShots\\" + fileName + "_" + recuperaDataArquivo() + ".jpeg"));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void criarDiretorio() {
		new File("C:\\SeleniumScreenShots\\").mkdir();
	}

	public static String recuperaDataArquivo() {
		SimpleDateFormat formatoData = new SimpleDateFormat("ddMMyyyy");
		Calendar data = Calendar.getInstance();
		return formatoData.format(data.getTime());

	}

	public static void aguardaValoresAjax() {
		try {
			Thread.sleep(5500l);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	public static boolean isNumeric(String s) {
		try {
			Long.parseLong(s);
			return true;
		} catch (NumberFormatException ex) {
			return false;
		}
	}

	public static String onlyNumbers(String str) {

		if (str != null) {

			return str.replaceAll("[^0123456789]", "");

		} else {

			return "";

		}

	}
}
