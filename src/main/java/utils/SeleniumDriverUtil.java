package utils;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public abstract class SeleniumDriverUtil {

	private static final String START_MAXIMIZED = "start-maximized";
	private static final String WEBDRIVER_IE_DRIVER = "webdriver.ie.driver";
	private static final String WEBDRIVER_GECKO_DRIVER = "webdriver.gecko.driver";
	private static final String WEBDRIVER_CHROME_DRIVER = "webdriver.chrome.driver";
	private static WebDriver driver = null;
	private static String browser = PropertyUtil.BROWSER_NAME;

	public static WebDriver getDriver() {

		if (driver == null) {
			prepararCriacaoDriverBrowser();
		}
		return driver;
	}

	private static void prepararCriacaoDriverBrowser() {
		if (BrowserUtil.CHROME.equals(browser)) {
			criarDriverChrome();
		} else if (BrowserUtil.IE.equals(browser)) {
			criarDriverInternetExplorer();
		} else if (BrowserUtil.FIREFOX.equals(browser)) {
			criarDriverFireFox();
		}
	}

	private static void criarDriverChrome() {
		configurarSystemProperty(WEBDRIVER_CHROME_DRIVER,
				recuperarCaminhoDriverBrowser(PropertyUtil.CHROME_DRIVE_PATH));
		ChromeOptions options = new ChromeOptions();
		options.addArguments(START_MAXIMIZED);
		driver = new ChromeDriver();
	}

	private static void criarDriverFireFox() {
		configurarSystemProperty(WEBDRIVER_GECKO_DRIVER,
				recuperarCaminhoDriverBrowser(PropertyUtil.FIREFOX_DRIVE_PATH));
		driver = new FirefoxDriver();
	}

	private static void criarDriverInternetExplorer() {
		DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
		capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		configurarSystemProperty(WEBDRIVER_IE_DRIVER, recuperarCaminhoDriverBrowser(PropertyUtil.FIREFOX_DRIVE_PATH));
	}

	private static String recuperarCaminhoDriverBrowser(String fileBrowser) {
		File file = new File(fileBrowser);
		return file.getAbsolutePath();
	}

	private static void configurarSystemProperty(String driver, String caminhoDriver) {
		System.setProperty(driver, caminhoDriver);
	}

}
