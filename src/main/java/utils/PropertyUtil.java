package utils;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

public abstract class PropertyUtil {

	public static String CHROME_DRIVE_PATH;
	public static String IE64_DRIVE_PATH;
	public static String FIREFOX_DRIVE_PATH;
	public static final String BROWSER_NAME;
	public static final String SITE_ADDRESS;
	public static final String LOGIN;
	public static final String SENHA;

	private static final String PROPERTIES_FILE = "config.properties";
	private static final String PROPERTIES_MENSAGENS = "mensagens_pt_BR.properties";

	static {
		CHROME_DRIVE_PATH = new File("").getAbsolutePath() + "\\src\\main\\resources\\drivers\\chromedriver.exe";
		IE64_DRIVE_PATH = new File("").getAbsolutePath() + "\\src\\main\\resources\\drivers\\IEDriverServer.exe";
		FIREFOX_DRIVE_PATH = new File("").getAbsolutePath() + "\\src\\main\\resources\\drivers\\geckodriver.exe";
		BROWSER_NAME = get("browser.name");
		SITE_ADDRESS = get("site.address");
		LOGIN = get("login");
		SENHA = get("senha");
	}

	private static String get(String name) {
		Properties properties = new Properties();
		String value = null;
		try {
			properties.load(PropertyUtil.class.getClassLoader().getResourceAsStream(PROPERTIES_FILE));
			value = properties.getProperty(name);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return value;
	}

	public static String getMsg(String name) {
		Properties properties = new Properties();
		String value = null;
		try {
			properties.load(PropertyUtil.class.getClassLoader().getResourceAsStream(PROPERTIES_MENSAGENS));
			value = properties.getProperty(name);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return value;
	}
}
