package resources;

import org.junit.AfterClass;
import org.junit.BeforeClass;

public class BaseTestcase {

	@BeforeClass
	public static void beforeClass() throws Exception {
		if (!AbstractLogin.isLogado) {
			AbstractLogin.prepararLogin();
		}
	}

	@AfterClass
	public static void afterClass() throws Exception {
		if (!AllTests.isAllTestsExecution) {
			AbstractLogin.driver.quit();
		}

	}

}
