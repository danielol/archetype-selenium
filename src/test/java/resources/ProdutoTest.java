package resources;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import pages.ProdutoPage;
import utils.PropertyUtil;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProdutoTest extends BaseTestcase {

	@Before
	public void before() throws Exception {

	}

	@Test
	public void pesquisarProjetocomSucesso() {
		ProdutoPage produtoPage = AbstractLogin.homePage.selecionaMenuConsultaProduto();
		produtoPage.preencheCamposPesquisa();
	}

	@Test
	public void inclusaoProjetoComSucesso() {
		ProdutoPage produtoPage = AbstractLogin.homePage.selecionaMenuConsultaProduto();
		produtoPage.cadastraProdutos();
	}

	@Test
	public void validarCampoObrigatorioDoCadastro() {
		ProdutoPage produtoPage = AbstractLogin.homePage.selecionaMenuConsultaProduto();
		produtoPage.validandoCamposObrigatoriosDoCadastro();
		assertEquals("Campo obrigatorio fail", produtoPage.pegandoMensagemValidacao(),
				PropertyUtil.getMsg("mensagem.produto.erro.codigo.produto"));
	}

	@Test
	public void validarSalvarCodigoProdutoExistente() {
		ProdutoPage produtoPage = AbstractLogin.homePage.selecionaMenuConsultaProduto();
		produtoPage.pegandoMensagemValidacaoCodigoProdutoExistente();
		assertEquals("Campo codigo produto existente fail",
				produtoPage.pegandoMensagemValidacaoCodigoProdutoExistente(),
				PropertyUtil.getMsg("mensagem.produto.cadastro.codigo.duplicado"));
	}

	@After
	public void after() throws Exception {
	}

}
