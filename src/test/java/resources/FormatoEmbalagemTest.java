package resources;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import pages.FormatoEmbalagemPage;
import utils.PropertyUtil;

public class FormatoEmbalagemTest extends BaseTestcase {

	FormatoEmbalagemPage formatoEmbalagemPage;

	@Before
	public void before() throws Exception {
		formatoEmbalagemPage = AbstractLogin.homePage.navegarParaFormatoEmbalagemPage();
	}

	@Test
	public void navegarParaPaginaEmbalagem() {
		assertTrue(formatoEmbalagemPage.recuperarLabelTitulo());
	}

	@Test
	public void validarLabelTipoEmabalagem() {
		assertThat("Label incorreta", formatoEmbalagemPage.recuperarLabelFiltroTipoEmbalagem(),
				is(PropertyUtil.getMsg("label.formatoEmbalagem.filtro.tipoEmbalagem")));
	}

	@After
	public void after() throws Exception {
	}

}
