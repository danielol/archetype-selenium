package resources;

import org.openqa.selenium.WebDriver;

import pages.HomePage;
import pages.LoginPage;
import utils.PropertyUtil;
import utils.SeleniumDriverUtil;

public abstract class AbstractLogin {

	protected static boolean isLogado = false;
	protected static WebDriver driver;
	public static LoginPage loginPage;
	public static HomePage homePage;

	public static void prepararLogin() {
		driver = SeleniumDriverUtil.getDriver();
		loginPage = new LoginPage(driver);
		loginPage.visita(PropertyUtil.SITE_ADDRESS);
		isLogado = true;
		homePage = loginPage.autenticar(PropertyUtil.LOGIN, PropertyUtil.SENHA);
	}
}
