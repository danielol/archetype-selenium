package resources;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import pages.FaixaDeNumeracaoPage;
import utils.PropertyUtil;

public class FaixaDeNumeracaoTest extends BaseTestcase {

	FaixaDeNumeracaoPage faixaPage;

	@Before
	public void before() throws Exception {
		faixaPage = AbstractLogin.homePage.selecionaMenuFaixaNumeracao();
	}

	@Test
	public void ValidaCampoObrigatorio() {
		faixaPage = AbstractLogin.homePage.selecionaMenuFaixaNumeracao();
		assertEquals("campo obrigatório", PropertyUtil.getMsg("mensagem.faixa.numeracao.erro.contrato"),
				faixaPage.retornandoMsgErroValidacao());
	}

	@Test
	public void ValidaLabel() {

	}
}
