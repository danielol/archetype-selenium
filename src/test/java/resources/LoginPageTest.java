package resources;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LoginPageTest extends BaseTestcase {

	@Before
	public void before() throws Exception {
	}

	@Test
	public void loginComSucesso() {
		assertTrue(AbstractLogin.homePage.isValida());
	}

	@After
	public void after() throws Exception {
		
	}

}
