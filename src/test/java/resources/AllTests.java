package resources;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ LoginPageTest.class, FormatoEmbalagemTest.class, FaixaDeNumeracaoTest.class

})

public class AllTests {

	public static Boolean isAllTestsExecution = false;

	@BeforeClass
	public static void beforeClass() throws Exception {
		isAllTestsExecution = true;
	}

	@AfterClass
	public static void afterClass() throws Exception {
		AbstractLogin.driver.quit();
	}

}
